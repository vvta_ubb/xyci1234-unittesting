package maxScoreCounter;

import maxScoreCounter.repository.Repository;
import maxScoreCounter.service.QuizException;
import maxScoreCounter.service.QuizService;
import maxScoreCounter.ui.QuizUI;
import maxScoreCounter.validation.QuizValidator;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
       String fileName = "data/quizzes.txt";

        Repository repo = null;
        QuizValidator validator =new QuizValidator();
        try {
            repo = new Repository(fileName, validator);
        } catch (IOException e) {
            e.printStackTrace();
        }

        QuizService ctrl = new QuizService(repo);

       QuizUI console = new QuizUI(ctrl);
        try {
            console.run();
        } catch (QuizException e) {
            e.printStackTrace();
        }
    }
}
