package maxScoreCounter.tests;

import maxScoreCounter.service.QuizException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import maxScoreCounter.repository.Repository;
import maxScoreCounter.service.QuizService;
import maxScoreCounter.validation.QuizValidator;

import static org.junit.Assert.assertEquals;

public class QuizServiceTest2 {

    private QuizService service;

    @Before
    public void setUp(){
        QuizValidator validator= new QuizValidator();
        Repository repo = new Repository(validator);
        service = new QuizService(repo);
    }

    //path coverage TC2
    @Test
    public void testWBT_invalid_maxScoreQuizCounter() throws QuizException {
        //maxScoreCounter.service.addQuiz(new Quiz("q1", 10, "Easy", 0));

        assertEquals("Expected value is -1 instead of  " +service.maxScoreQuizCounter(), -1, service.maxScoreQuizCounter());
    }
    @After
    public void tearDown(){
        service = null;
    }
}